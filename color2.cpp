// NOTE: compile with g++ filename.cpp -std=c++11
 
#include <iostream>
#include <cmath>
#define DIM 1024
#define DM1 (DIM-1)
#define _sq(x) ((x)*(x))                           // square
#define _cb(x) abs((x)*(x)*(x))                    // absolute value of cube
#define _cr(x) (unsigned char)(pow((x),1.0/3.0))   // cube root
#define r(n)(rand()%n)
 
unsigned char GR(int,int);
unsigned char BL(int,int);

#define W 41
#define DR 20
#define DG 85
#define DB 112
#define RAD 256
 
unsigned char RD(int i,int j){
    float d = _sq(i-512)+_sq(j-512);
    if(cos(d/(_sq(2)*1024)) > 0.5) return DR; 
    return (i/W)%2 && (j/W)%2 ? DR : 255;
}
unsigned char GR(int i,int j){
    float d = _sq(i-512)+_sq(j-512);
    if(cos(d/(_sq(2)*1024)) > 0.5) return DR; 
    return (i/W)%2 || (j/W)%2 ? DG : 255;
}
unsigned char BL(int i,int j){
    float d = _sq(i-512)+_sq(j-512);
    if(cos(d/(_sq(2)*1024)) > 0.5) return DR; 
    return (i/W)%2 || (j/W)%2 ? DB : 255;
}
 
void pixel_write(int,int);
FILE *fp;
int main(){
    fp = fopen("MathPic","wb");
    fprintf(fp, "P6\n%d %d\n255\n", DIM, DIM);
    for(int j=0;j<DIM;j++)
        for(int i=0;i<DIM;i++)
            pixel_write(i,j);
    fclose(fp);
    return 0;
}
void pixel_write(int i, int j){
    static unsigned char color[3];
    color[0] = RD(i,j)&255;
    color[1] = GR(i,j)&255;
    color[2] = BL(i,j)&255;
    fwrite(color, 1, 3, fp);
}
